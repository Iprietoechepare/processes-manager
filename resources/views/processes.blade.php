<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="create-tab" data-bs-toggle="tab" data-bs-target="#create" type="button" role="tab" aria-controls="create" aria-selected="true">Create Process</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="list-tab" data-bs-toggle="tab" data-bs-target="#list" type="button" role="tab" aria-controls="list" aria-selected="false">List Process</button>
            </li>
        </ul>
        <div class="tab-content" id="app">
            <div class="tab-pane fade" role="tabpanel" id="create" aria-labelledby="create-tab">
                <form>
                    <div class="mb-3">
                        <label for="type" class="form-label">Type</label>
                        <select class="form-select" id="type" v-model="type">
                            <option disabled="disabled">Choose the type</option>
                            <option v-for="option in options" v-bind:value="option.value">@{{ option.text }}</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="input" class="form-label">Input</label>
                        <textarea class="form-control" id="input" v-model="input"></textarea>
                    </div>
                    <button type="button" class="btn btn-primary" v-on:click="create">Create</button>
                    <button type="button" class="btn btn-primary" v-on:click="createAndRun">Create and Run</button>
                </form>
            </div>
            <div class="tab-pane fade show active" role="tabpanel" id="list" aria-labelledby="list-tab">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Process ID</th>
                            <th scope="col">Input</th>
                            <th scope="col">Output</th>
                            <th scope="col">Created at</th>
                            <th scope="col">Started at</th>
                            <th scope="col" style="width: 110px;">Finished at</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="process in processes" :key="process.id">
                            <th scope="col">#</th>
                            <td scope="col">@{{ process.id }}</td>
                            <td scope="col">@{{ process.input }}</td>
                            <td scope="col">@{{ process.output }}</td>
                            <td scope="col">@{{ process.created_at }}</td>
                            <td scope="col">@{{ process.started_at }}</td>
                            <td scope="col">@{{ process.finished_at }}</td>
                            <td scope="col">@{{ process.status }}</td>
                            <td scope="col">#</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script type="text/javascript">
        const app = new Vue({
            el: '#app',
            data() {
                return {
                    processes: null,
                    options: [
                      { text: 'Vowels Count', value: 'VOWELS_COUNT' },
                      { text: 'Words Count', value: 'WORDS_COUNT' }
                    ],
                    type: null,
                    input: null
                }
            },
            mounted () {
                axios
                .get('http://processes-manager.test/api/v1/process')
                .then(response => {
                    this.processes = response.data.results
                })
            },
            methods: {
                create: () => {
                    axios.post('http://processes-manager.test/api/v1/process', 
                    {
                        "type": this.type.value,
                        "input": this.input.value,
                        "start": false
                    })
                    .then((response) => {
                        app.processes.push(response.data.results)
                    })
                },
                createAndRun: () => {
                    axios.post('http://processes-manager.test/api/v1/process', 
                    {
                        "type": this.type.value,
                        "input": this.input.value,
                        "start": true
                    })
                    .then((response) => {
                        app.processes.push(response.data.results)
                    })
                }
            }
        });
    </script>
</body>
</html>
