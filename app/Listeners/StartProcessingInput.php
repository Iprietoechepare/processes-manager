<?php

namespace App\Listeners;

use App\Events\ProcessStarted;
use App\Models\Process;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StartProcessingInput //implements ShouldQueue
{
    // use InteractsWithQueue;
    /**
     * Handle the event.
     *
     * @param  ProcessStarted  $event
     * @return void
     */
    public function handle(ProcessStarted $event)
    {
        $process = $event->process;
        $vowels_count = $this->vowelsCount($process);
        $process->output = $vowels_count;
        $process->finished_at = Carbon::now();
        $process->status = Process::FINISHED;
        $process->save();
    }

    private function vowelsCount($process)
    {
        // Aquí haría unas clases con cada tipo de proceso y las haría respetar un contrato y todas se ejecutarían con ->handle() y cada una haría sus cosas. $process->type -> sería el nombre de la clase
        preg_match_all('/[aeiou]/i', $process->input, $matches);
        return count($matches[0]);
    }
}
